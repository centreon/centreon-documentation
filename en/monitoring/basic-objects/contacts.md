---
id: contacts
title: Contacts
---

In Centreon, contacts can:

* Receive [notifications](../../alerts-notifications/notif-configuration.html).
* Log in to the Centreon web interface: each contact has its own [rights](../../administration/access-control-lists.html) to connect to the web interface.

You can:
- [Create contacts manually](contacts-create.html).
- [Connect your Centreon to an LDAP directory](../../administration/parameters/ldap.html).