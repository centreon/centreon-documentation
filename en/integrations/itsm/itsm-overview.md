---
id: itsm-overview
title: ITSM
---

Transform one or several alerts into case(s) in your favorite ITSM tool. 
Doing so allows for advanced incident management in a dedicated tool, and to seamlessly 
integrate Centreon monitoring alerts in your existing processes.

Available providers:

* [BMC Footprints](ot-bmc-footprints.html)
* [BMC Remedy](ot-bmc-remedy.html)
* [EasyVista](ot-easyvista.html)
* [GLPI](ot-glpi.html)
* [GLPI RestAPI](ot-glpi-restapi.html)
* [iTop](ot-itop.html)
* [IWS Isilog](ot-iws-isilog.html)
* [Jira](ot-jira.html)
* [Mail](ot-mail.html)
* [OTRS RestAPI](ot-otrs-restapi.html)
* [Request Tracker RestAPI](ot-request-tracker-restapi.html)
* [Serena](ot-serena.html)
* [ServiceNow](ot-servicenow.html)