---
id: introduction
title: Centreon Platform 21.10
---

Vous trouverez dans ce chapitre la note de version globale de la **Plateforme Centreon 21.10**.

Pour accéder aux notes de version détaillées par composants, rendez vous dans les sections suivantes:

- [Core](centreon-core.html)
- [Extensions commerciales](centreon-commercial-extensions.html)
- [Extensions Open Source](centreon-os-extensions.html)
