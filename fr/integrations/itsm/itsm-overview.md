---
id: itsm-overview
title: ITSM
---

Ouvrez des tickets depuis une ou plusieurs alertes détectées par Centreon
afin de permettre une gestion de l'incident et des actions correctives au plus proche 
de vos processus internes.

Les interconnexions disponibles sont les suivantes :

* [BMC Footprints](ot-bmc-footprints.html)
* [BMC Remedy](ot-bmc-remedy.html)
* [EasyVista](ot-easyvista.html)
* [GLPI](ot-glpi.html)
* [GLPI RestAPI](ot-glpi-restapi.html)
* [iTop](ot-itop.html)
* [IWS Isilog](ot-iws-isilog.html)
* [Jira](ot-jira.html)
* [Mail](ot-mail.html)
* [OTRS RestAPI](ot-otrs-restapi.html)
* [Request Tracker RestAPI](ot-request-tracker-restapi.html)
* [Serena](ot-serena.html)
* [ServiceNow](ot-servicenow.html)