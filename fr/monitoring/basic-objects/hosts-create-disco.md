---
id: hosts-create-disco
title: Créer des hôtes automatiquement
---

Utilisez la [fonctionnalité de découverte automatique d'hôtes](../discovery/introduction.html) pour [détecter des hôtes et les créer automatiquement](../discovery/hosts-discovery.html) dans Centreon.

L'autodécouverte se fait via des [Plugin Packs](../pluginpacks.html).

La fonctionnalité d'autodécouverte demande une [licence](../../administration/licenses.html) valide.