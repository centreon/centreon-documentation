---
id: contacts
title: Les contacts
---

Dans Centreon, les contacts :

* Peuvent recevoir des [notifications](../../alerts-notifications/notif-configuration.html).
* Peuvent se connecter à l’interface web de Centreon : chaque contact dispose de ses propres [droits](../../administration/access-control-lists.html) afin de se
  connecter à l’interface web.

Vous pouvez :
- [créer des utilisateurs manuellement](contacts-create.html) 
- [connecter votre Centreon à un annuaire LDAP](../../administration/parameters/ldap.html).