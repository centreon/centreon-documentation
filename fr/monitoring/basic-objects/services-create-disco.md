---
id: services-create-disco
title: Créer des services automatiquement
---

Les services peuvent être créés automatiquement de plusieurs façons :

- Lorsque vous [créez un hôte manuellement](hosts-create.html) en utilisant un [Plugin Pack](../pluginpacks.html), et que vous cochez la case **Créer aussi les services liés aux modèles**, les services associés à l'hôte sont créés automatiquement. 

- En utilisant la [fonctionnalité de découverte automatique de services](../discovery/introduction.html) pour [détecter des services et les créer automatiquement](../discovery/services-discovery.html) dans Centreon.

    - L'autodécouverte se fait via des [Plugin Packs](../pluginpacks.html).

    - La fonctionnalité d'autodécouverte demande une [licence](../../administration/licenses.html) valide.